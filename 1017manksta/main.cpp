#include <unordered_map>
#include <algorithm>
#include <vector>
#include <fstream>
#include <string>

struct DataSet
{
    typedef std::pair<std::string, int> Pair;
    
    std::unordered_map<std::string, Pair> Data;
    
    void AddOrUpdate(const std::string& name, int value)
    {
        auto itr = Data.find(name);
        if(itr == Data.end())
        {
            Data.insert({name, Pair(name, value)});
        }
        else
        {
            itr->second.second += value;
        }
    }
    
    DataSet(std::ifstream& stream)
    {
        int trash;
        stream >> trash;
        
        while(stream)
        {
            std::string name;
            int num;
            
            stream >> name;
            stream >> num;
        
            AddOrUpdate(name, num);
        }
    }
    
    std::vector<Pair> Sort()
    {
        auto vec = std::vector<Pair> {};

        for(const auto& kvp : Data)
        {
            if(kvp.first.size() != 0)
                vec.push_back(kvp.second);
        }    
        std::sort(vec.begin(), vec.end(), [](const Pair& a, const Pair& b) { return a.second > b.second; });
        return vec;
    }
};

int main()
{
    auto input = std::ifstream("../U2duom.txt");
    auto output = std::ofstream("../U2rez.txt");
    auto data = DataSet(input);
    
    auto sol = data.Sort();
    
    for(const auto& val : sol)
    {
        output << val.first << " " << val.second << std::endl;
    }
}