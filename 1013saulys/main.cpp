#include <algorithm>
#include <vector>
#include <fstream>
#include <string>
#include <iterator>

struct Person
{
    std::string Name = { };
    float Average;
    
    friend std::istream& operator>>(std::istream& io, Person& data)
    {
        auto s = std::istream::sentry(io);
        if(!s) return io;
        data = Person();
        
        while(isspace(io.peek())) io.get();
        
        for(int i = 0; i < 20; i++)
            data.Name.push_back(io.get());
            
        int shots = 0;
        int score = 0;
        for(int i = 0; i < 10; i++)
        {
            int buf;
            io >> buf;
            score += (9 - i) * buf;
            shots += buf;
        }
        
        data.Average = (float)score / (float)shots;
        
        return io;
    }
};

int main()
{
    int trash;
    auto input = std::ifstream("../duom.txt");
    auto output = std::ofstream("../rez.txt");
    auto ppl = std::vector<Person>(std::istream_iterator<Person>(input), {});    
    input >> trash;
    
    std::sort(ppl.begin(), ppl.end(), [](const Person& a, const Person& b) { return a.Average > b.Average; });
    
    auto upper = std::min((size_t)3, ppl.size());
    
    for(int i = 0; i < upper; i++)
    {
        Person& person = ppl.at(i);
        output << person.Name << " " << person.Average << std::endl;
    }
}