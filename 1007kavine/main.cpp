#include <vector>
#include <fstream>
#include <iomanip>
#include <string>
#include <utility>
#include <cctype>
#include <algorithm>
#include <assert.h>

/* Find the most expensive order, pretty print it */

constexpr static int NAME_SIZE = 24;

std::string ReadStringOfSize(std::istream& stream, int size)
{
    assert(size > 0);
    
    char name[size] = {};
    
    // skip any prefixed newlines we may have
    while(true)
    {
        char c = stream.peek();
        if(c != '\n' && c != '\r')
            break;
            
        stream.get();
    }
    
    // read name
    int lastCharPos = 0;
    for(int i = 0; i < size; ++i)
    {
        char c = stream.get();

        if(c == '\n' || c == '\r')
            break;
            
        if(c != ' ')
            lastCharPos = i;
            
        name[i] = c;
    }
    
    // add a null term so that the string ctor doesn't 
    // freak out about any traling trash data array members
    if(lastCharPos < size-1)
        name[lastCharPos+1] = '\0';
        
    return std::string(name);
}

    typedef std::pair<std::string, int> Item;

struct Solution;

struct Order
{
    std::vector<int> Raw;
    int Price;
    
    Order(const std::vector<Item>& items, int count, std::ifstream& str);
    
    void Serialize(const char* where);
};

Order::Order(const std::vector<Item>& items, int count, std::ifstream& str) : 
    Raw(),
    Price(0)
{
    for(int i = 0; i < count; ++i)
    {
        int times;
        str >> times;
    
        Price += times * items.at(i).second;
        
        Raw.push_back(times);
    }
}

struct DataSet
{

    
    std::vector<Item> Items;
    std::vector<Order> OrderTable;
    
    DataSet(std::ifstream& stream);
    Order& Solve();
    
    void Serialize(const Order& what, const char* where);
};


DataSet::DataSet(std::ifstream& stream)
{
    int itemCount, orderCount;
    stream >> itemCount >> orderCount;
    
    for(int i = 0; i < itemCount; ++i)
    {
        std::string name = ReadStringOfSize(stream, NAME_SIZE);
        int price;
        stream >> price;
        Items.push_back(Item(name, price));
    }
    
    for(int i = 0; i < orderCount; ++i)
        OrderTable.push_back(Order(Items, itemCount, stream));
}

Order& DataSet::Solve()
{
    return *std::max_element(OrderTable.begin(), OrderTable.end(),
    [](const Order& rhs, const Order& lhs)
    {
        return rhs.Price < lhs.Price;
    });
}

void DataSet::Serialize(const Order& what, const char* where)
{
    std::ofstream stream(where);
    assert(stream.is_open());
    
    #define TBL_PRINT(str, a, b, c) \
        stream << std::setw(NAME_SIZE) << std::left << str << std::right << std::setw(2) << a << " X " << b << " = " << std::right << std::setw(2) << c << std::endl
    
    
    for(unsigned i = 0; i < what.Raw.size(); ++i)
    {
        auto& item = Items.at(i);
        TBL_PRINT(item.first, item.second, what.Raw.at(i), (item.second * what.Raw.at(i)));
    }
    
    #undef TBL_PRINT
    
    stream << "Is viso........................." << std::right << std::setw(3) << what.Price << std::endl;
}

int main()
{
    std::ifstream stream("../kav_duom.txt");
    assert(stream.is_open());
    DataSet data(stream);
    data.Serialize(data.Solve(), "../kav_rez.txt");
    
    return 0;
}