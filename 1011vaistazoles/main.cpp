#include <fstream>
#include <iterator>

int main()
{
    auto input = std::ifstream("../vaist_duom.txt");
    auto output = std::ofstream("../vaist_rez.txt");
    
    int days, size, leftovers, maxFull = 0;
    input >> days >> size;

    #define PRINT(a) output << a << std::endl
    for(auto itr = std::istream_iterator<int>(input); itr != std::istream_iterator<int>(); itr++)
    {
        auto boxes = *itr / size;
        auto remainder = *itr % size;
        if(boxes > maxFull) maxFull = boxes;
        leftovers += remainder;
            
        PRINT(boxes << " " << remainder);
    }
    
    PRINT(leftovers / size);
    PRINT(maxFull);
}