#include <fstream>
#include <map>
#include <iostream>
#include <assert.h>
#include <vector>

inline std::vector<int> Deserialize(const char* fromWhere)
{
    std::ifstream stream(fromWhere);
    assert(stream.is_open());
    
    int size;
    stream >> size;
    
    std::vector<int> data;
    for(int i = 0; i < size; ++i)
    {
        int buf;
        stream >> buf;
        data.push_back(buf);
    }
    
    return data;
}

inline std::vector<int> Solve(const std::vector<int>& data)
{
    std::map<int, int> table;
    std::vector<int> prod;
    
    for(int val : data)
    {
        table[val]++;
    }
    
    for(const auto& kvp : table)
    {
        if(kvp.second % 2 != 0) prod.push_back(kvp.first);
    }
    
    return prod;
}

inline void Serialize(const char* toWhere, const std::vector<int>& solution)
{
    std::ofstream stream(toWhere);
    for(int val : solution)
    {
        stream << val;
    }
}

int main()
{
    auto data = Deserialize("../vienas_duom.txt");
    auto prod = Solve(data);
    
    Serialize("../vienas_rez.txt", prod);
    
    return 0;
}
