/* Daugiausiai panautota (max negative delta)
 * Visai nepanaudotus (no delta)
 * Maziausias likutis (min cumulative delta)
 */
 
#include <vector>
#include <fstream>
#include <memory>
#include <string>
#include <assert.h>
#include <cmath>
#include <iomanip>
#include <iostream>
 
class MutableStock
{
    int _timesUsed;
    int _stock;

public:
    
    std::string Name;
    
    MutableStock(const std::string& name, int initialStock) :
        _timesUsed(0),
        _stock(initialStock),
        Name(name)
    { }
    
    void ApplyDelta(int delta)
    {
        if(0 > delta)
        {
            _timesUsed += std::abs(delta);
        }
        
        _stock += delta;
    }
    
    inline bool WasUsed() const { return _timesUsed > 0; }
    inline int GetStock() const { return _stock; }
    inline int GetTimesUsed() const { return _timesUsed; }
};

struct Solution;

struct DataSet
{
    std::shared_ptr<std::vector<MutableStock>> Stock;
    
    DataSet(const char* fromWhere);
 
    std::shared_ptr<Solution> Solve();
};

struct Solution
{
    const std::shared_ptr<std::vector<MutableStock>> Stock;
    
    int MostUsed;
    int LeastLeft;
    std::vector<int> Unused;
    
    Solution(const DataSet& data) :
        Stock(data.Stock),
        MostUsed(0),
        LeastLeft(0),
        Unused()
    { }
    
    void Serialize(const char* where);
};

DataSet::DataSet(const char* fromWhere) :
    Stock(std::make_shared<std::vector<MutableStock>>())
{
    std::ifstream stream(fromWhere);
    assert(stream.is_open());

    int count;
    stream >> count;
        
    // deserialize initial stock
    for(int stockIdx = 0; stockIdx < count; stockIdx++)
    {
        static int SIZE = 24;
        char name[SIZE] = {};
        
        // skip any prefixed newlines we may have
        while(true)
        {
            char c = stream.peek();
            if(c != '\n' && c != '\r')
                break;
                
            stream.get();
        }
        
        // read name
        int lastCharPos = 0;
        for(int i = 0; i < SIZE; ++i)
        {
            char c = stream.get();

            if(c == '\n' || c == '\r')
                break;
                
            if(c != ' ')
                lastCharPos = i;
                
            name[i] = c;
        }
        
        // add a null term so that the string ctor doesn't 
        // freak out about any traling trash data array members
        if(lastCharPos < SIZE-1)
            name[lastCharPos+1] = '\0';
    
        int numStock;
        stream >> numStock;
        
        auto strName = std::string(name);
        
        Stock->push_back(MutableStock(name, numStock));
    }
    
    stream >> count;
    
    // deserialize piece table
    for(int i = 0; i < count; i++)
    {
        int index, delta;
        stream >> index >> delta;
        index--; // convert 1-starting idx to 0-starting idx
        
        assert(index < Stock->size());
        
        Stock->at(index).ApplyDelta(delta);
    }
}

std::shared_ptr<Solution> DataSet::Solve()
{
    auto sol = std::make_shared<Solution>(*this);
    
    for(int i = 0; i < Stock->size(); ++i)
    {
        MutableStock& cur = Stock->at(i);
        if(cur.WasUsed())
        {
            MutableStock& mostUsed = Stock->at(sol->MostUsed);
            if(cur.GetTimesUsed() > mostUsed.GetTimesUsed())
                sol->MostUsed = i;
                
            MutableStock& leastLeft = Stock->at(sol->LeastLeft);
            if(cur.GetStock() < leastLeft.GetStock())
                sol->LeastLeft = i;
        }
        else
        {
            sol->Unused.push_back(i);
        }
    }
    
    return sol;
}

void Solution::Serialize(const char* where)
{
    std::ofstream stream(where);
    assert(stream.is_open());
    
    
    
    stream  << "Automobiliu taisykla \"Voztuvas\"" << std::endl
            << "Detaliu apyvarta" << std::endl;
            
    #define HEADER(str) \
        stream << str << std::endl << "==================================" << std::endl
        
    #define PRINT(stock, getter) \
        stream << std::left << std::setw(30) << stock.Name << std::right << std::setw(2) << stock.getter << std::endl
        
    #define END() \
        stream << std::endl

    HEADER("Daugiausiai panaudotos");
    
    MutableStock& mostUsed = Stock->at(MostUsed);
    PRINT(mostUsed, GetTimesUsed());
    
    END();
    HEADER("Nepanaudotos");
    
    for(int idx : Unused)
    {
        MutableStock& unused = Stock->at(idx);
        PRINT(unused, GetStock());
    }
    
    END();
    HEADER("Maziausias likutis");
    MutableStock& leastUsed = Stock->at(LeastLeft);
    
    PRINT(leastUsed, GetStock());
    
    #undef HEADER
    #undef PRINT
}

int main()
{
    DataSet data("../detales_duom.txt");
    auto solution = data.Solve();
    solution->Serialize("../detales_rez.txt");
    
    return 0;
}