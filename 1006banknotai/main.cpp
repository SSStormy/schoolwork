/*
 * Kiek, kokiu nominalu buvo:
 *   * priimta
 *   * isduota
 * 
 * Kasos galutinis balansas
 * Pinigu delta (su +-)
 * 
 */

#include <stdio.h>
#include <array>
#include <cmath>
#include <vector>
#include <fstream>
#include <assert.h>
#include <iomanip>

class ImmutableMoney
{

public:
    constexpr static int DENOMINATION_COUNT     = 9;
    typedef std::array<int, DENOMINATION_COUNT> Storage;
    
    static const Storage MODS;
    
    ImmutableMoney() : _buckets() { }
    ImmutableMoney(std::ifstream& stream);
    ImmutableMoney(int initialValue) :
        _buckets()
    { 
        Calc(initialValue); 
    }
    
    const Storage& GetDenomiators() const;
    
private:
    Storage _buckets;
    void Calc(int value);
    
};

const ImmutableMoney::Storage& ImmutableMoney::GetDenomiators() const { return _buckets; }

void ImmutableMoney::Calc(int value)
{
    int idxDenomiator = DENOMINATION_COUNT - 1;
    
    while(value)
    {
        int denominator = MODS.at(idxDenomiator);
        int whole       = value / denominator;
        
        _buckets.at(idxDenomiator) = whole;
        
        value -= whole * denominator;
        idxDenomiator--;
    }
}

ImmutableMoney::ImmutableMoney(std::ifstream& stream)
{
    int value = 0;
    for(int i = 0; i < DENOMINATION_COUNT; ++i)
    {
        int temp;
        stream >> temp;
        value += temp * MODS[i];
    }
    
    Calc(value);
}

const ImmutableMoney::Storage ImmutableMoney::MODS = { 1, 2, 5, 10, 20, 50, 100, 200, 500 };

struct CashRegister
{
    ImmutableMoney::Storage InitialValues;
    ImmutableMoney::Storage Balance;
    ImmutableMoney::Storage Given;
    ImmutableMoney::Storage Taken;

    CashRegister(const ImmutableMoney::Storage& initialValues) :
        InitialValues(initialValues),
        Balance(),
        Given(),
        Taken()
    { 
        
    }

    void Apply(const ImmutableMoney& money)
    {
        auto data = money.GetDenomiators();
        for(int i = 0; i < ImmutableMoney::DENOMINATION_COUNT; ++i)
        {
            auto num = data[i];
            if(0 > num)
                Given[i] += std::abs(num);
            else
                Taken[i] += num;
                
            Balance[i] += num;
        }
            
    }
};

#define FOR_DENOMINATOR(_x) for(int _x = 0; _x < ImmutableMoney::DENOMINATION_COUNT; ++_x)

struct DataSet
{
    ImmutableMoney InitialMoney;
    std::vector<int> PieceTable;
    
    DataSet(const char* fromFile);
    
    CashRegister Solve();
};

static int SumDenominators(const ImmutableMoney::Storage& storage)
{
    int cash = 0;
    
    FOR_DENOMINATOR(i)
        cash += storage[i] * ImmutableMoney::MODS[i];
 
    
    return cash;
}

DataSet::DataSet(const char* fromFile) :
        InitialMoney(),
        PieceTable()
{
    std::ifstream stream(fromFile);
    assert(stream.is_open());
    
    InitialMoney = ImmutableMoney(stream);
    
    int count;
    stream >> count;
    for(int i = 0; i < count; ++i)
    {
        int temp;
        stream >> temp;
        PieceTable.push_back(temp);
    }
}

CashRegister DataSet::Solve()
{
    CashRegister reg(InitialMoney.GetDenomiators());
    
    for(int piece : PieceTable)
    {
        reg.Apply(ImmutableMoney(piece));
    }
    
    return reg;
}

void Serialize(const char* where, const CashRegister& reg)
{
    std::ofstream stream(where);
    assert(stream.is_open());
    
    #define SEPARATOR \
        stream << "=====================" << std::endl
        
    #define NUM_PRINT(a, b, c) \
        stream << std::right << std::setw(3) << a << " X " << std::right << std::setw(3) << b << " = " << std::right << std::setw(6) << c << " Lt" << std::endl
        
    #define TEXT(str) \
        stream << std::left << str << std::endl
        
    #define TEXT_NUM(str, n) \
        stream << std::left << str << std::right << std::setw(10) << n << " Lt" << std::endl
    
    auto serializeDenominators = [&](ImmutableMoney::Storage val)
    {
        SEPARATOR;
        
        int money = 0;
        FOR_DENOMINATOR(i)
        {
            auto value = ImmutableMoney::MODS.at(i) * val.at(i);
            if(value != 0)
            NUM_PRINT(ImmutableMoney::MODS.at(i), val.at(i), value);
            money += value;
        }
        
        SEPARATOR;
        
        TEXT_NUM("Is viso:", money) << std::endl;
    };
    

    TEXT("Priimta:");
    serializeDenominators(reg.Taken);
    
    TEXT("Isduota:");
    serializeDenominators(reg.Given);
    
    int balance = SumDenominators(reg.Balance);
    stream << std::left << "Balansas:" << std::right << std::setw(5) << ((balance >= 0) ? "+" : "-") << balance << " Lt " << std::endl;
    #undef SEPARATOR
    #undef NUM_PRINT
    #undef TEXT
}

int main(int argc, char **argv)
{
    DataSet data("../banknotai_duom.txt");
    Serialize("../banknotai_rez.txt", data.Solve());
    
	return 0;
}
