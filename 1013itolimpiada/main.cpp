#include <fstream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <iostream>

int numPoints;
struct Person
{
    std::string Name = {};
    int Score = 0;
    
    friend std::istream& operator>>(std::istream& io, Person& data)
    {
        // ensure we're reading into an empty Person 
        data = Person();
        
        // sentry
        auto s = std::istream::sentry(io);
        if(!s) return io;

        // skip ws
        while(isspace(io.peek())) io.get();
        
        // istreambuf_iterator does not want to work here and i have no idea why
        //std::copy_n(std::istreambuf_iterator<char>(io), 16, std::back_inserter(data.Name));
        
        for(int i = 0; i < 16; i++)
            data.Name.push_back(io.get());
        
        auto values = std::vector<int>();
        std::copy_n(std::istream_iterator<int>(io), 4, std::back_inserter(values));
        data.Score = std::accumulate(values.begin(), values.end(), 0);
        
        return io;
    }
};

int main()
{
    auto input = std::ifstream("../olimp_duom.txt");
    auto output = std::ofstream("../olimp_rez.txt");
    #define PRINT(a) output << a << std::endl
    
    int numPeople, minPoints = 0;
    input >> numPeople >> numPoints >> minPoints;

    auto ppl = std::vector<Person>(std::istream_iterator<Person>(input), {});
    
    std::sort(ppl.begin(), ppl.end(), [](const Person& a, const Person& b) { return a.Score > b.Score; });

    int numWinners = 0;
    for(int i = 0; i < numPeople; ++i)
    {
        if(i == 3) PRINT("--------------------");
        if(i == 5) PRINT("====================");
        PRINT(ppl.at(i).Name << " " << ppl.at(i).Score);
        if(ppl.at(i).Score >= minPoints) ++numWinners;
    }
    
    PRINT(numWinners);
}
