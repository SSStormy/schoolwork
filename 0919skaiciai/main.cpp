#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>

struct WordModel
{    
    std::string Word;
    int NumEvenNumbers;
};

#define FAIL(msg) { std::cout << msg << std::endl; std::exit(1); }

std::vector<std::string> read()
{
    std::ifstream input("../skaiciai_duom.txt");
    if(!input.is_open()) 
    {
        FAIL("Failed opening input file")
    }
    
    unsigned elements;
    input >> elements;
    
    std::vector<std::string> retval;
    for(unsigned i = 0; i < elements; i++)
    {
        std::string buf;
        input >> buf;
        retval.push_back(buf);
    }
    
    return retval;
}

std::vector<WordModel> buildModel(const std::vector<std::string>& data)
{
    std::vector<WordModel> retval;
    
    for(const std::string& str : data)
    {
        WordModel model;
        model.Word = str;
        model.NumEvenNumbers = 0;
        
        for(const auto ch : str)
        {
            int num = ch - '0';
            if(num % 2 == 0) ++model.NumEvenNumbers;
        }
        
        retval.push_back(model);
    }
    
    std::sort(retval.begin(), retval.end(), [](const WordModel& a, const WordModel& b)
    {
       return a.NumEvenNumbers > b.NumEvenNumbers; 
    });
    
    return retval;
}

void write(const std::vector<WordModel>& model)
{
    if(model.size() == 0) return;
    
    std::ofstream output("skaiciai_rez.txt");                                                                                   
    
    const WordModel& target = model.at(0);
    int printed = 0;
    
    if(target.NumEvenNumbers > 0)
    {
        for(const auto& mdl : model)
        {
            if(target.NumEvenNumbers <= mdl.NumEvenNumbers)
            {
                output << mdl.Word << std::endl;    
                ++printed;
            }
        }
    }
    
    if(0 >= printed)
    {
        output << "NERA" << std::endl;
    }
}

int main()
{
    auto data = read();
    auto model = buildModel(data);
    write(model);
    
	return 0;
}
