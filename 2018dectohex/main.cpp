#include <fstream>
#include <string>
#include <sstream>
\
std::string AsHex(int val)
{
    const char* letters = "0123456789ABCDEF";
    int whole = val / 16;
    int rem = val % 16;
    std::stringstream stream;
    stream << letters[whole];
    stream << letters[rem];
    
    return stream.str();
}

int main()
{
    auto input = std::ifstream("../U1duom.txt");
    auto output = std::ofstream("../U1rez.txt");
    int x, y;
    
    input >> x >> y;
    
    while(x--)
    {
        auto iy = y;
        while(iy--)
        {
            for(int i = 0; i < 3; i++)
            {
                int val;
                input >> val;
                output << AsHex(val);
            }
            
            if(iy)
                output << ";";
        }
        output << std::endl;
    }
}