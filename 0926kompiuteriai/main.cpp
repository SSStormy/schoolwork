#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <assert.h>
#include <algorithm>
#include <memory>
#include <iomanip>

struct DataProduct;

class Computer
{
    int _score = -1;
    
public:
    
    float ProcessorSpeed_Ghz;
    int RAM_GB;
    int HDD_GB;
    int Wattage;
    int Price;
    int Warranty_Month;
    
    bool ProcessorSpeed_GhzSatisfied = false;
    bool RAM_GBSatisfied = false;
    bool HDD_GBSatisfied = false;
    bool WattageSatisfied = false;
    bool PriceSatisfied = false;
    bool Warranty_MonthSatisfied = false;

    float PriceToQuality;
    
    Computer(float proc, int ram, int hdd, int watts, int price, int warranty) : 
        ProcessorSpeed_Ghz(proc),
        RAM_GB(ram),
        HDD_GB(hdd),
        Wattage(watts),
        Price(price),
        Warranty_Month(warranty)
    {
        PriceToQuality = Price / (ProcessorSpeed_Ghz + RAM_GB + (HDD_GB / 1000.0f) - (Wattage / 1000.0f) + (Warranty_Month / 20.0f));
    }
    
    void WithScore(const Computer& ref);
    int GetScore() const { return _score; }
    
    static Computer ReadFrom(std::istream& stream);
};

struct Vendor
{
    std::string Name;
    Computer Product;
    
    Vendor(const std::string& name, const Computer& prod) :
        Name(name),
        Product(prod)
    { }
};

struct DataSet
{
    std::vector<Vendor> Vendors;
    Computer Target;
    
    DataSet(std::ifstream& str);
    
    DataProduct Solve();
};

struct DataProduct
{
    std::shared_ptr<Vendor> Winner;
    void Serialize(const char* toWhere);
};


DataSet::DataSet(std::ifstream& stream) :
    Vendors(),
    Target(Computer::ReadFrom(stream))
{
    int vendorCount;
    stream >> vendorCount;
    
    for(int i = 0; i < vendorCount; ++i)
    {
        std::string name;
        stream >> name;
        auto pc = Computer::ReadFrom(stream);
        pc.WithScore(Target);
        
        Vendors.push_back(Vendor(name, pc));
    }
}

Computer Computer::ReadFrom(std::istream& stream)
{
    float ghz;
    int ram, hdd, watt, price, warranty;
    stream >> ghz >> ram >> hdd >> watt >> price >> warranty;
    return Computer(ghz, ram, hdd, watt, price, warranty);
}

void Computer::WithScore(const Computer& ref)
{
    _score = 0;
    #define COMPARE(memb, comparer) { if(memb comparer ref.memb) { memb##Satisfied = true; _score++; } else { memb##Satisfied = false; } }

    COMPARE(ProcessorSpeed_Ghz, >=)
    COMPARE(RAM_GB, >=)
    COMPARE(HDD_GB, >=)
    COMPARE(Wattage, <=)
    COMPARE(Price, <=)
    COMPARE(Warranty_Month, >=)

    #undef COMPARE
}

DataProduct DataSet::Solve()
{
    auto set = Vendors;
    
    assert(set.size() > 0);
    
    // filter out products which do not have a score over or equal to MIN_SCORE
    
    std::remove_if(set.begin(), set.end(),
        [](const Vendor& vendor)
        {
            static constexpr int MIN_SCORE = 5;
            return vendor.Product.GetScore() < MIN_SCORE;
        });
        
    if(set.size() <= 0)
    {
        DataProduct prod;
        prod.Winner = nullptr;
        return prod;
    }
    
    // sort products by best price to quality ratio
    std::sort(set.begin(), set.end(), [](const Vendor& lhs, const Vendor& rhs)
    {
       return lhs.Product.PriceToQuality > rhs.Product.PriceToQuality;
    });
    
    DataProduct prod;
    prod.Winner = std::make_shared<Vendor>(set.at(0));
    return prod;
}

void DataProduct::Serialize(const char* toWhere)
{
    std::ofstream stream(toWhere);
    if(Winner != nullptr)
    {
        Computer& pc = Winner->Product;
        stream << "Pirksime is parduotuves " << Winner->Name << std::endl;
        stream << "====================================" << std::endl;
        #define PRINT(msg, var) \
            stream << std::left << std::setw(30) << msg << std::setw(5) << pc.var << ((pc.var##Satisfied) ? "tenkina" : "netenkina") << std::endl 
        
        PRINT("Taktinis daznis GHz", ProcessorSpeed_Ghz);
        PRINT("RAM dydis GB", RAM_GB);
        PRINT("Standziojo disko dydis GB", HDD_GB);
        PRINT("Elektrine galia W", Wattage);
        PRINT("Kaina Lt", Price);
        PRINT("Garantija men.", Warranty_Month);
        
        #undef PRINT
    }
    else
    {
        stream << "Ne viena parduotuve nepateike tinkamo pasiulymo.";
    }
}

int main()
{
    std::ifstream stream("../kompiuteriai_duom.txt");
    DataSet set(stream);
    
    auto prod = set.Solve();
    prod.Serialize("../kompiuteriai_rez.txt");
    
    return 0;
}