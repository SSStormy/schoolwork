#include <fstream>
#include <vector>
#include <assert.h>
#include <algorithm>
#include <string>
#include <unordered_map>
#include <iomanip>
#include <unordered_set>

struct PlayerHandle
{
    std::string Callcard;
    std::string Nationality;
    std::string Name;
    int Id;
    
    PlayerHandle() {}
    PlayerHandle(std::ifstream& stream);
    
    bool operator==(const PlayerHandle& rhs) const { return Callcard == rhs.Callcard; }
};

namespace std
{    
    template<>
    struct hash<PlayerHandle>
    {
        std::size_t operator()(const PlayerHandle& rhs) const { return std::hash<std::string>{}(rhs.Callcard); }
    };
};


PlayerHandle::PlayerHandle(std::ifstream& stream)
{
    stream >> Callcard >> Name;
    
    // parse callcard
    Nationality = Callcard.substr(0, 2);
    Id = std::stoi(Callcard.substr(2, Callcard.size() - 2));
}

class Game;

struct Connection
{
    int TimeEstablished;
    PlayerHandle ToWho;
    int TimeReceived;
        
    Connection(std::ifstream& stream);
};

Connection::Connection(std::ifstream& stream)
{ 
    stream >> TimeEstablished;
    ToWho = PlayerHandle(stream);
    stream >> TimeReceived;
}

struct Player
{
    PlayerHandle Handle;
    std::vector<Connection> Connections;
    int Index;
    
    Player(int index, std::ifstream& stream);
    
    int GetPoints(Game& context, const Connection& connection) const;
    int GetNationalityPoints(const PlayerHandle& oth) const;
};

struct GhostPlayer
{
    std::unordered_set<int> ReceiveSlots;
};

Player::Player(int index, std::ifstream& stream) :
    Handle(stream),
    Connections(),
    Index(index)
{
    int count;
    stream >> count;
    
    for(int i = 0; i < count; ++i)
    {
        Connections.push_back(Connection(stream));
    }
}

struct Game
{
    std::unordered_map<PlayerHandle, Player> Players;
    std::unordered_map<std::string, GhostPlayer> Ghosts;
    
    Game(std::ifstream& stream);
};

Game::Game(std::ifstream& stream) :
    Players(),
    Ghosts()
{
    int count;
    stream >> count;
    
    for(int i = 0; i < count; ++i)
    {
        Player player(i+1, stream);
        Players.insert({player.Handle, player});
    }
}

struct Solution
{
    typedef std::pair<Player, int> Pair;
    std::vector<Pair> Scoreboard;
    
    Solution(Game& game);
    
    void Serialize(const char* where);
};

int Player::GetNationalityPoints(const PlayerHandle& oth) const
{
    if(oth.Nationality == Handle.Nationality)
        return 1;
    else 
        return 5;
}

int Player::GetPoints(Game& context, const Connection& connection) const
{
    auto itr = context.Players.find(connection.ToWho);
    
    if(itr == context.Players.end())
    {
        // try ghost players
        auto ghostItr = context.Ghosts.find(connection.ToWho.Callcard);
        GhostPlayer* ghost;
        
        // add ghost if doesn't exist
        if(ghostItr == context.Ghosts.end())
        {
            context.Ghosts.insert({connection.ToWho.Callcard, GhostPlayer()});
            ghost = &context.Ghosts.at(connection.ToWho.Callcard);
        }
        else // ghost exists
            ghost = &ghostItr->second;

        // check if the receive slot is occupied
        if(ghost->ReceiveSlots.count(connection.TimeReceived) > 0)
            return 0;
        
        // not occupied, then mark as occupied
        ghost->ReceiveSlots.insert(connection.TimeReceived);
        
        // return score
        return GetNationalityPoints(connection.ToWho);
    }
    
    const Player& other = itr->second;
    
    // find a connection
    for(unsigned i = 0; i < other.Connections.size(); ++i)
    {
        const Connection& othConnection = other.Connections.at(i);
        
        // compare so ToWho, then compare this.Establish and other.Receive time.
        if(othConnection.ToWho == Handle &&
            connection.TimeEstablished == othConnection.TimeReceived)
        {
            return GetNationalityPoints(other.Handle);
        }
    }
    
    return 0;
}

Solution::Solution(Game& game) :
    Scoreboard()
{
    for(const auto& kvp : game.Players)
    {
        const Player& player = kvp.second;
        int score = 0;
        
        for(const Connection& connection : player.Connections)
        {
            score += player.GetPoints(game, connection);
        }
        
        Scoreboard.push_back({player, score});
    }
    
    std::sort(Scoreboard.begin(), Scoreboard.end(),
    [](const Pair& a, const Pair& b)
    {
        return a.second > b.second;
    });
}

void Solution::Serialize(const char* where)
{
    std::ofstream stream(where);
    assert(stream.is_open());
    
    for(const Pair& pair : Scoreboard)
    {
        const Player& player = pair.first;
        const int& score = pair.second;
        stream << 
            std::left << std::setw(2) << player.Index << 
            std::left << std::setw(6) << player.Handle.Callcard << 
            std::left << std::setw(10) << player.Handle.Name << 
            std::right << std::setw(6) << score << std::endl;
    }
}

int main()
{
    std::ifstream stream("../Radio_duom.txt");
    assert(stream.is_open());
    Game game(stream);
    
    Solution solution(game);
    solution.Serialize("../Radio.rez.txt");
    
    return 0;
}
