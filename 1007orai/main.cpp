/*
 *   Number of days when  t > t0 and p0 >= p
 */

#include <fstream>
#include <vector>
#include <assert.h>
#include <string>
#include <sstream>
#include <utility>
#include <memory>

struct Weather
{
    int Temperature;
    int Pressure;
    
    Weather(std::ifstream& str);
  
    bool SatisfiesCriteria(const Weather& criteria) const
    {
        return Temperature > criteria.Temperature && criteria.Pressure >= Pressure;
    }
  
};


struct DataSet
{
    Weather Target;
    std::vector<Weather> Data;
    
    DataSet(std::ifstream& str);
    int Solve();
};

Weather::Weather(std::ifstream& str)
{
    str >> Temperature;
    str >> Pressure;
}

DataSet::DataSet(std::ifstream& str) :
    Target(str),
    Data()
{
    int count;
    str >> count;
    
    for(int i = 0; i < count; ++i)
        Data.push_back(Weather(str));
}

int DataSet::Solve()
{
    int retval = 0;
    for(const Weather& weather : Data)
    {
        if(weather.SatisfiesCriteria(Target))
            ++retval;
    }
    
    return retval;
}

std::unique_ptr<std::ifstream> GetStream(int num)
{
    std::stringstream strStream;
    strStream << "../orai_duom" << num << ".txt";
    
    auto stream = std::make_unique<std::ifstream>(strStream.str());
    assert(stream->is_open());
    
    return std::move(stream);
}

int main()
{
    auto str = GetStream(2);
    DataSet data(*str);
    
    std::ofstream output("../orai_rez.txt");
    assert(output.is_open());
    output << data.Solve();
    
    return 0;
}