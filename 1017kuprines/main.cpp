/*
 * No structs, arrays,
 * No iostream
 * func which returns the number of backpacks that are less than 0.5*max
 * func which returns the heaviest bp
 * no div
*/

#include <iterator>
#include <fstream>

#define STREAM_LOOP(stream) int _loop; stream >> _loop; while(_loop--)

int FindMaxBp(std::ifstream& stream)
{
    int max = 0;
    STREAM_LOOP(stream)
    {
        int temp;
        stream >> temp;
        if(temp > max)
            max = temp;
    }
    
    return max;
}

int FindNumSatisfyingQuery(std::ifstream& stream, int max)
{
    float query = max * 0.5f;
    
    int amnt = 0;
    STREAM_LOOP(stream)
    {
        int temp;
        stream >> temp;
        if(temp <= query)
            ++amnt;
    }
    return amnt;
}

int main()
{
    // since we can't use arrays or data structures, we'll have to
    // iterate the input file twice.
    auto input = std::ifstream("../U1duom.txt");
    auto max = FindMaxBp(input);
    input.clear();
    input.seekg(0);
    
    auto val = FindNumSatisfyingQuery(input, max);
    
    auto output = std::ofstream("../U1rez.txt");
    output << max << " " << val << std::endl;
    
    
}