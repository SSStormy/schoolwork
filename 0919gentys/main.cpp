#include <vector>
#include <iostream>
#include <fstream>
#include "assert.h"
#include <algorithm>

struct DataModel
{
    std::vector<int> Currency;
    std::vector<int> Product;
    
    void ReadFromFile();
    int Solve();
};   

void FillRead(std::ifstream& stream, int num, std::vector<int>& vec)
{
    for(unsigned int i = 0; i < num; ++i)
    {
        int temp;
        stream >> temp;
        vec.push_back(temp);
    }
}

void DataModel::ReadFromFile()
{
    std::ifstream input("../gentys_duom.txt");
    assert(input.is_open());
    int numCurrency, numProduct;
    
    input >> numCurrency >> numProduct;
    FillRead(input, numCurrency, Currency);
    FillRead(input, numProduct, Product);
}

int DataModel::Solve()
{
    static constexpr int COST = 3;
    std::vector<int> spent;
    for(int money : Currency)
    {
        for(int numProduct : Product)
        {
            // clamp product to multiple of COST, round down.
            int clampedProduct = numProduct - (numProduct % COST);
            int buyPower = money * COST;
            spent.push_back(std::min(clampedProduct, buyPower) / COST);
        }
    }
    
    std::sort(spent.begin(), spent.end());
    return spent[spent.size() - 1];
}

void Serialize(int product)
{
    std::ofstream stream("../gentys_rez.txt");
    stream << product;
}

int main()
{
    DataModel model;
    model.ReadFromFile();
    Serialize(model.Solve());
    
    return 0;
}
