#include <fstream>
#include <map>
#include <iterator>
#include <algorithm>
#include <vector>

struct Rect
{
    unsigned XStart; 
    unsigned XEnd;
    unsigned YStart;
    unsigned YEnd;
    
    std::array<unsigned, 3> Colors;
    
    friend std::istream& operator>>(std::istream& io, Rect& data)
    {
        auto s = std::istream::sentry(io);
        if(!s) return io;
        data = Rect();
        
        io >> data.XStart >> data.YStart;
        
        unsigned temp;
        io >> temp;
        data.XEnd = data.XStart + temp;
        
        io >> temp;
        data.YEnd = data.YStart + temp;
        
        for(int i = 0; i < 3; i++)
        {
            io >> data.Colors.at(i);
        }

        return io;
    }    
};

struct Canvas
{
    typedef std::pair<unsigned, unsigned> Pair;

    std::map<Pair, std::array<unsigned, 3>> ColorMap;
    
    Pair UpperBound;
    
    Canvas() : 
        ColorMap(),
        UpperBound(0,0)
    {
        
    }
    
    void Blit(const Rect& r)
    {
        for(unsigned y = r.YStart; y < r.YEnd; y++)
        {
            for(unsigned x = r.XStart; x < r.XEnd; x++)
            {
                auto pair = Pair(x,y);
                if(ColorMap.count(pair) == 0)
                    ColorMap.insert({pair, r.Colors});
                else
                    ColorMap.at(pair) = r.Colors;
                    
                if(pair.first > UpperBound.first || pair.second > UpperBound.second)
                    UpperBound = pair;
            }
        }
    }
};

int main()
{
    auto input = std::ifstream("../U2duom.txt");
    auto output = std::ofstream("../U2rez.txt");
    unsigned val;
    input >> val;
    
    auto canvas = Canvas();
    
    auto vec = std::vector<Rect>();
    std::copy_n(std::istream_iterator<Rect>(input), val, std::back_inserter(vec));
    std::for_each(vec.begin(), vec.end(), [&](const Rect& r) { canvas.Blit(r); });
    
    output << canvas.UpperBound.second + 1 << " " << canvas.UpperBound.first + 1 << std::endl;
    
    for(unsigned y = 0; y <= canvas.UpperBound.second; y++)
    {
        for(unsigned x = 0; x <= canvas.UpperBound.first; x++)
        {
            auto pair = Canvas::Pair(x ,y);
            if(canvas.ColorMap.count(pair) == 0)
                for(int i = 0; i < 3; i++)
                    output << 255 << " ";
            else
            {
                auto& val = canvas.ColorMap.at(pair);
                
                for(int i = 0; i < 3; i++)
                    output << val.at(i) << " ";
            }
            output << std::endl;
        }
    }
}