/* Multi. Kuriuose reisuose daugiausiai zmoniu (zm skaicius ir reiso idx)
 * Multi. Kada autobusas buvo pilniausias (zm skaicius, reiso idx, stoteles vardas)
 * Multi. Kada ilipo daugiausiai zmoniu (skaicius, stoteles)
 * Multi. islipo daugiausiai
 * Kiek islipo zmoniu po pamainos
 */
 
#include <vector>
#include <algorithm>
#include <fstream>
#include <iterator>

struct TripData
{
    std::vector<int> Boarded = {};
    std::vector<int> Left = {};
    int StationIndex = 0;
    int TotalBoarded = 0;
    int TotalLeft = 0;
    
    TripData() { }
    
    TripData(std::ifstream& io, int numStations, int index)
        : StationIndex(index)
    {
        for(int i = 0; i < numStations; i++)
        {
            int board, leave;
            io >> board >> leave;
            
            Boarded.push_back(board);
            Left.push_back(leave);
            
            TotalBoarded += board;
            TotalLeft += leave;
        }
    }
};

int main()
{
    auto input = std::ifstream("../auto_duom.txt");
    auto output = std::ofstream("../auto_rez.txt");
    auto stations = std::vector<std::string>();
    auto trips = std::vector<TripData>();
    int numTrips, numStations;
    
    input >> numTrips >> numStations;
    std::copy_n(std::istream_iterator<std::string>(input), numStations, std::back_inserter(stations));
    for(int i = 0; i < numTrips; i++)
        trips.push_back(TripData(input, numStations, i));
        
    std::sort(trips.begin(), trips.end(), [](const TripData& a,const TripData& b) { return a.TotalBoarded > b.TotalBoarded; });
    
    TripData& prev = trips.at(0);
    output << "Daugiausia keleiviu " << prev.TotalBoarded << std::endl;
    for(const TripData& trip : trips)
    {
        if(prev.TotalBoarded == trip.TotalBoarded)
            output << prev.StationIndex;
        else 
            break;
    }
    
    
    /*
    auto timesMostFull = std::vector<std::pair<int, int>>(); // number, station index
    TripData& maxPeople;
    
    int pplInBus = 0;
    for(const TripData& trip : trips)
    {
        for(int i = 0; i < numStations; i++)
        {
            pplInBus += trip.Boarded.at(i);
            timesMostFull.push_back(std::pair<int, int>(pplInBus, i));
            pplInBus -= trip.Left.at(i);
        }
    }
    
    std::sort(timesMostFull.begin(), timesMostFull.end(), [](const std::pair<int, int>& a, const std::pair<int, int>& b) return a.first > b.second; })
     */
}