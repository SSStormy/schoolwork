#include <vector>
#include <memory>

struct AbstractNode
{
    std::shared_ptr<Node> Left;
    std::shared_ptr<Node> Right;
    
    virtual int GetValue() = 0;
};

struct ValueNode: public AbstractNode
{
    int Value = 0;
    
    int GetValue() override { return Value; }
};


float CalcSeries(float a, float b)
{
    return a + b;
}

float CalcParallel(float a, float b)
{
    return (a + b)/ a * b;
}

struct ConnectorNode : public AbstractNode
{
    bool IsParallel = false;
    int GetValue() 
    {
        if(IsParallel)
        {
            return CalcParallel(Left->GetValue(), Right()->GetValue());
        }
        else
        {
            return CalcSeries(Left->GetValue(), Right()->GetValue());
        }
    }
};

struct ResistorData
{
    bool InUse;
    float Value;
    
    ResistorData(float value) :
        InUse(false),
        Value(value) { }
};

bool RecursiveSolve(int target, std::vector<ResistorData> resistors, Node<int>& node)
{
    for(int left = 0; left < resistors.size(); left++)
    {
        ResistorData& resistorLeft = resistors.at(left);
        
        if(resistorLeft.InUse)
            continue;
            
        resistorLeft.InUse = true;
        
        for(int right = 0; right < resistors.size(); right++)
        {
            if(left == right) 
                continue;
                
            ResistorData& resistorRight = resistors.at(right);
            
            if(resistorRight.InUse)
                continue;
                
            if(valueLeft - CalcSeries(valueLef))
        }
        
        // TODO : set resistorLeft.InUse = false;
    }
}

int main()
{
    auto resistors = std::vector<ResistorData> { 1, 2, 2, 4, 5 ,6 };
    Node<int> root;
    
    RecursiveSolve(root);
    
    return 0;
}