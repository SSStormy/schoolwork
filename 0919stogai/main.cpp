#include "assert.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

struct DataModel;
struct DataProduct;

struct DataModel
{
    std::vector<std::pair<int, int>> TileMeasurements;
    int Width;
    int Height;
    
    void ReadFromFile();
    DataProduct Solve() const;
};

struct DataProduct
{
    std::pair<int, int> Measurements;
    int NumTiles;
    float Remainders;
    
    void OutputToFile() const;
};

void DataModel::ReadFromFile()
{
    std::ifstream input("../stogas_duom.txt");
    assert(input.is_open());
    
    int count, w,h;
    input >> count;
    
    for(unsigned i = 0; i < count; ++i)
    {
        input >> w >> h;
        TileMeasurements.push_back({w,h});
    }
    
    input >> Width >> Height; 
}

DataProduct DataModel::Solve() const
{
    assert(TileMeasurements.size() > 0);
        
    std::vector<DataProduct> products;
    
    for(const auto kvp : TileMeasurements)
    {
        float tilesX = (float)Width / (float)kvp.first;
        float tilesY = (float)Height / (float)kvp.second;
        
        int intTilesX = ceil(tilesX);
        int intTilesY = ceil(tilesY);
    
        int numTiles = tilesX * tilesY;
        
        DataProduct product;
        product.Measurements = kvp;
        product.NumTiles = intTilesX * intTilesY;
        product.Remainders = ((float)intTilesX - tilesX) + ((float)intTilesY - tilesY);

        products.push_back(product);
    }
    
    std::sort(products.begin(), products.end(),[](const DataProduct& rhs, const DataProduct& lhs)
    {
        return rhs.Remainders < lhs.Remainders;
    });
    
    return products.at(0);
}

void DataProduct::OutputToFile() const
{
    std::ofstream output("stogas_rez.txt");
    assert(output.is_open());
    
    output << Measurements.first << " " << Measurements.second << " " << NumTiles;
}

int main()
{
    DataModel model;
    model.ReadFromFile();

    auto prod = model.Solve();
    prod.OutputToFile();

    return 0;
}
