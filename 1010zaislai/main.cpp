#include <vector>
#include <fstream>
#include <array>
#include <algorithm>

struct Toy
{
    std::string Name;
    int Produced = 0;
};

int main()
{
    auto input = std::ifstream("../zaislai_duom.txt");
    auto totalDayProduction = std::array<int, 7>{};
    int numDays, numToys = 0;
    auto toys = std::vector<Toy>(numToys);
    
    input >> numDays >> numToys;
    
    for(int i = 0; i < numToys; i++)
    {
        Toy t;
        input >> t.Name;
        toys.push_back(t);
    }
        
    for(int iDay = 0; iDay < numDays; iDay++)
    {
        int dayIndex;
        input >> dayIndex;
        dayIndex--;
        
        for(int iToy = 0; iToy < numToys; iToy++)
        {
            int temp;
            input >> temp;
            
            toys.at(iToy).Produced += temp;
            totalDayProduction.at(dayIndex) += temp;
        }
    }
    
    auto output = std::ofstream("../zaislai_rez.txt");
    int toyProdAccum = 0;
    
    for(const Toy& toy : toys)
    {
        output << toy.Name << " " << toy.Produced << std::endl;
        toyProdAccum += toy.Produced;
    }
    
    output << toyProdAccum << std::endl;
    
    int idxMostProduciveDay = std::max_element(totalDayProduction.begin(), totalDayProduction.end()) 
                    - totalDayProduction.begin();
    auto dayNames = std::vector<const char*> 
    {
        "Pirmadieni",
        "Antradieni",
        "Treciadieni",
        "Ketvirtadeni",
        "Penktadieni",
        "Sestadieni",
        "Sekmadieni"
    };
    
    output << dayNames.at(idxMostProduciveDay) << std::endl;
    
    Toy& mostProducedToy = *std::max_element(toys.begin(), toys.end(), [](const Toy& a, const Toy& b) 
    {
        return a.Produced < b.Produced;
    });
    
    output << mostProducedToy.Name << std::endl;
    
}
