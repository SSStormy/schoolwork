#include <vector>
#include <iomanip>
#include <fstream>
#include <assert.h>
#include <string>

/*
 * Kiek individai uzdirbo
 * Kiek visi totaliai uzdirbo
 */
 

struct Money
{
    int Lt;
    int Ct;
    
    Money() : Lt(0), Ct(0) { }
    Money(std::ifstream& stream)
    {
        stream >> Lt >> Ct;
    }
    
    void Normalize()
    {
        Lt += Ct / 100;
        Ct %= 100;
    }
};

struct Worker
{
    std::string Name;
    Money Pay;
    
    Worker(std::ifstream& stream)
    {
        stream >> Name;
        Pay = Money(stream);
    }
};
 
struct DataSet
{
    std::vector<Worker> Workers;
    std::vector<std::vector<int>> PieceTable;
    
    DataSet(std::ifstream& stream) :
        Workers(),
        PieceTable()
    {
        int numWorkers, numDays;
        stream >> numWorkers >> numDays;
        
        for(int i = 0; i < numWorkers; ++i)
            Workers.push_back(Worker(stream));
        
        for(int i = 0; i < numDays; ++i)
        {
            std::vector<int> vec;
            for(int workIdx = 0; workIdx < numWorkers; ++workIdx)
            {
                
                int buf;
                stream >> buf;
                vec.push_back(buf);
            }
            
            PieceTable.push_back(vec);
        }
    }
};

struct Solution
{
    const DataSet& Data;
    std::vector<Money> Results;
    
    Solution(const DataSet& data) :
        Data(data),
        Results(data.Workers.size())
    {
        for(const std::vector<int> day : data.PieceTable)
        {
            for(int i = 0; i < day.size(); ++i)
            {
                const Worker& worker = data.Workers.at(i);
                Money& pay = Results.at(i);
                
                pay.Lt += worker.Pay.Lt * day.at(i);
                pay.Ct += worker.Pay.Ct * day.at(i);
            }
        }
        
        // normalize
        for(auto& r : Results)
        {
            r.Normalize();
        }
    }
    
    void Serialize(const char* where)
    {
        std::ofstream stream(where);
        assert(stream.is_open());
        
        Money total;
        for(int i = 0; i < Results.size(); ++i)
        {
            const Worker& worker = Data.Workers.at(i);
            const Money& pay = Results.at(i);
            
            stream << worker.Name << pay.Lt << " Lt " << pay.Ct << " ct" << std::endl;
            
            total.Lt += pay.Lt;
            total.Ct += pay.Ct;
        }
        
        total.Normalize();
        stream << total.Lt << " " << total.Ct << std::endl;
    }
};
 
int main()
{
    std::ifstream stream("../brigada_duom.txt");
    assert(stream.is_open());
    DataSet data(stream);
    Solution sol(data);
    sol.Serialize("../brigada_rez.txt");
    
    return 0;
}